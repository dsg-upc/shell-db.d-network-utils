#!/bin/sh -e

. ./pool_status.sh
. ./validation.sh

main() {
  # validation
  all_vlan_validation
  vlan_on_hosts_validation

  # pool status
  total_vlan_usage_ipv4
}

main
